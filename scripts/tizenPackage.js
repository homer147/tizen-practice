const { spawn } = require('child_process');
const fs = require('fs');

function tizenDefineProfiles() {
    const defineProfile = spawn('tizen', ['cli-config', 'default.profiles.path=/Users/michaelbrowning/repos/tizen-practice/tizen/profiles.xml']);

    defineProfile.stdout.on('data', data => {
        console.log(data.toString());
    });

    defineProfile.on('close', () => {
        tizenPackage();
    });
}

function tizenPackage() {
    //const createPackage = spawn('tizen', ['package', '-t', 'wgt', '-s', 'tizenPractice', '--', './dist/.buildResult']);
    const createPackage = spawn('tizen', ['package', '-t', 'wgt', '--', './dist/.buildResult']);

    createPackage.stdout.on('data', data => {
        console.log(data.toString());
    });

    createPackage.on('close', () => {
        fs.rename('./dist/.buildResult/tizenPractice.wgt', './dist/tizenPractice.wgt', err => {
            if (err) {
                console.log('ERROR: ' + err);
            }
        });
    });
}

function tizenBuildWeb() {
    const buildWeb = spawn('tizen', ['build-web', '--', './dist/']);

    buildWeb.stdout.on('data', data => {
        console.log(data.toString());
    });

    buildWeb.on('close', () => {
        tizenDefineProfiles();
    });
}

module.exports = tizenBuildWeb();
