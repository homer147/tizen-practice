const spawn = require("child_process").spawn;
const chromeLauncher = require("chrome-launcher");
const fs = require("fs");

const tizenDeviceIp = process.argv.splice(process.execArgv.length + 2);
const PACKAGE_NAME = 'tizenPractice';
const PACKAGE_ID = 'oCNUETQ5WS.tizenPracticeApp';
const LOCAL_HOST = "127.0.0.1";
let connectedDevice = null;


// main functions

function connectToTizenDevice() {
    const cmd = spawn('sdb', ['connect', tizenDeviceIp]);
    let result = '';

    cmd.stdout.on('data', data => {
        console.log(data.toString());
        result += data.toString();
    });

    cmd.on('close', code => {
        if (code || result.includes('error')) {
            console.log('code: ' + code);
            return;
        }

        installPackageToDevice();
    });
}

async function installPackageToDevice() {
    let installPath = await getDeviceInstallPath();
    let result = '';

    await pushToDevice(installPath);
    await uninstallFromDevice();

    console.log(`INSTALL PATH: ${installPath}/${PACKAGE_NAME}.wgt`);

    const cmd = spawn('sdb', ['-s', connectedDevice, 'shell', '0', 'vd_appinstall', PACKAGE_NAME, `${installPath}/${PACKAGE_NAME}.wgt`]);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
        result += data.toString();
    });

    cmd.on('close', code => {
        // 'close' is an error message we have experienced when a package fails to install
        // so if this is found in the result string, we know an error was encounted.
        if (code || result.includes('close') || result.includes('failed')) {
            console.log('Error - Install failed');
            return;
        }

        launchOnDevice();
    });
}

function launchOnDevice() {
    const cmd = spawn('sdb', ['-s', connectedDevice, 'shell', '0', 'debug', PACKAGE_ID]);
    let result = '';

    cmd.stdout.on('data', data => {
        console.log(data.toString());
        result += data.toString();
    });

    cmd.on('close', code => {
        if (code) {
            return;
        }

        createDebugSession(result);
    });
}

async function createDebugSession(result) {
    const port = identifyPort(result);

    await removeExistingPortForwarding(port);
    await setupPortForwarding(port);

    chromeLauncher.launch({
        startingUrl: LOCAL_HOST + ":" + port,
        chromeFlags: ["--disable-web-security"]
    }).then((chrome) => {
        console.log("Chrome debugging port running on " + chrome.port);
    });
}


// helper methods

function uninstallFromDevice() {
    const cmd = spawn('sdb', ['-s', connectedDevice, 'shell', '0', 'vd_appuninstall', PACKAGE_NAME]);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Uninstall failed');
            }

            resolve(null);
        });
    });
}

async function getDeviceInstallPath() {
    connectedDevice = await getConnectedDevice();

    const cmd = spawn('sdb', ['-s', connectedDevice, 'capability']);
    let capabilities = {};
    let result = '';

    cmd.stdout.on('data', data => {
        result += data.toString();
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Could not get device capabilities');
            }

            let tempArr = result.split("\n");

            tempArr.map(value => {
                const splitVal = value.split(":");
                capabilities[splitVal[0]] = splitVal[1];
            });

            resolve(capabilities.sdk_toolpath);
        });
    });
}

function getConnectedDevice() {
    let devicesInfo = [];
    let deviceNameList = [];
    let result = '';

    cmd = spawn('sdb', ['devices']);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
        result += data.toString();
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Get connected device failed');
            }

            if (result.includes(tizenDeviceIp) && !result.includes("offline")) {
                devicesInfo = result.trim().split("\n");
                devicesInfo.shift();
                deviceNameList = identifyDeviceName(devicesInfo);
            }

            resolve(deviceNameList.find(i => i.includes(tizenDeviceIp)));
        });
    });
}

function identifyDeviceName(devices) {
    let deviceNameList = [];
    devices.forEach((device) => {
        deviceNameList.push(device.split("\t")[0].trim());
    });

    return deviceNameList;
};

function pushToDevice(installPath) {
    const cmd = spawn('sdb', ['-s', connectedDevice, 'push', `dist/${PACKAGE_NAME}.wgt`, installPath]);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Push to device failed');
            }

            resolve(null);
        });
    });
}

function identifyPort(str) {
    const startIndex = str.indexOf("port:") + 5;
    const port = str.slice(startIndex, str.length - 1).trim();

    return port;
}

function removeExistingPortForwarding(port) {
    const cmd = spawn('sdb', ['-s', connectedDevice, 'forward', '--remove', `tcp:${port}`]);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Could not remove port forwarding');
            }

            resolve(null);
        });
    });
}

function setupPortForwarding(port) {
    const cmd = spawn('sdb', ['-s', connectedDevice, 'forward', `tcp:${port}`, `tcp:${port}`]);

    cmd.stdout.on('data', data => {
        console.log(data.toString());
    });

    return new Promise((resolve, reject) => {
        cmd.on('close', code => {
            if (code) {
                reject('Error - Could not set up port forwarding');
            }

            resolve(null);
        });
    });
}

module.exports = connectToTizenDevice();
