const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const spawn = require('child_process').spawn;

const pathsToClean = ['dist'];

module.exports = {
    entry: "./src/index.js",
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean),
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        }),
        new CopyWebpackPlugin([
            './tizen/config.xml',
            './tizen/.project',
            './tizen/.tproject'
        ])
    ],
}
