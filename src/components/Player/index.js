import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import Controls from '../Controls';
import config from '../../configs/appConfig';
import AVPlayer from '../Devices/Tizen/AVPlayer';
import HTML5Player from '../Devices/Tizen/HTML5Player';
import {
    updatePlayerState,
    updateSeekingSpeed,
    updateControlsState,
    updateProgress,
    updateDuration
} from '../../actions';
import './styles.scss';

const PLAYER_STATE = config.playerState;
const SEEKING_SPEED = config.seekingSpeed;
const SEEKING_DIRECTION = config.seekingDirection;
const PLAYER_TYPE = config.playerType;
const CONTROLS_STATE = config.controlsState;
const video = config.video[config.mediaType];

class Player extends Component {
    constructor(props) {
        super(props);
        this.child = React.createRef();
        this._curTime = 0;
        this._duration = 0;
        this._seekingInterval = null;
        this._controlsTimeout = null;
        this._progressInterval = null;
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleKeyPress, true);
        this.showPlay();
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyPress, true);
        this.clearControlsTimeout();
        this.stopProgressInterval();
    }

    renderPlayer() {
        switch(PLAYER_TYPE) {
            case 'HTML5Player':
                return <HTML5Player video={video} ref={this.child} />;

            case 'AVPlayer':
            default:
                return <AVPlayer video={video} ref={this.child} />;
        }
    }

    showPlay() {
        this.props.updateControlsState(CONTROLS_STATE.STOPPED);
    }

    showControls() {
        this.props.updateControlsState(CONTROLS_STATE.VISIBLE);
        this.clearControlsTimeout();
        this._controlsTimeout = setTimeout(() => {
            this.props.updateControlsState(CONTROLS_STATE.INVISIBLE);
            this._controlsTimeout = null;
        }, 7000);
    }

    clearControlsTimeout() {
        if (this._controlsTimeout) {
            clearTimeout(this._controlsTimeout);
            this._controlsTimeout = null;
        }
    }

    handleKeyPress = (e) => {
        const { state, controlsState } = this.props.player;
        const mapKeys = this.child.current.getMapKeys();

        console.log('[Player] handleKeyPress state = ' + Object.keys(PLAYER_STATE).find(key => PLAYER_STATE[key] === state));

        switch(e.keyCode) {
            case mapKeys.enter:
            case mapKeys.playPause:
                if (state === PLAYER_STATE.PLAYING) {
                    this.pause();
                } else if (state === PLAYER_STATE.SEEKING) {
                    this.stopSeeking();
                } else {
                    this.play();
                }
                break;

            case mapKeys.back:
                if (controlsState === CONTROLS_STATE.VISIBLE) {
                    this.props.updateControlsState(CONTROLS_STATE.INVISIBLE);
                    this.clearControlsTimeout();
                } else if (state === PLAYER_STATE.STOPPED) {
                    this.exit();
                } else {
                    this.stop();
                }
                break;

            case mapKeys.arrowLeft:
                this.seek(SEEKING_DIRECTION.REWIND);
                break;

            case mapKeys.arrowUp:
                break;

            case mapKeys.arrowRight:
                this.seek(SEEKING_DIRECTION.FASTFORWARD);
                break;

            case mapKeys.arrowDown:
                this.showControls();
                break;

            default:
                console.log('unhandled key was pressed');
        }
    }

    startProgressInterval() {
        this.stopProgressInterval();
        this._progressInterval = setInterval(() => {
            this._curTime = this.child.current.getCurrentTime();

            if (this._curTime) {
                this.updateProgress();
            }
        }, 1000);
    }

    stopProgressInterval() {
        if (this._progressInterval) {
            clearInterval(this._progressInterval);
            this._progressInterval = null;
        }
    }

    updateProgress() {
        if (!this._duration) {
            this._duration = this.child.current.getDuration();
            this.props.updateDuration(this._duration);
        }
        //const duration = this.child.current.getDuration();
        //const percentage = (this._curTime / duration * 100).toFixed(2);

        this.props.updateProgress(this._curTime);
    }

    play() {
        console.log('[Player] play');

        this.showControls();
        this.props.updatePlayerState(PLAYER_STATE.PLAYING);
        this.child.current.play();
        this.startProgressInterval();
        //console.log(this.child.current.getDuration());
    }

    pause() {
        console.log('[Player] pause');

        this.stopProgressInterval();
        this.showControls();
        this.props.updatePlayerState(PLAYER_STATE.PAUSED);
        this.child.current.pause();
    }

    seek(direction) {
        console.log('[Player] seek');

        const { seekingSpeed, state } = this.props.player;
        const curSpeed = SEEKING_SPEED.indexOf(seekingSpeed);
        const speed = curSpeed < (SEEKING_SPEED.length - 1) ? SEEKING_SPEED[curSpeed + 1] : SEEKING_SPEED[SEEKING_SPEED.length - 1];
        const interval = 1000 / speed;

        this.stopProgressInterval();
        this.clearControlsTimeout();
        this.props.updateControlsState(CONTROLS_STATE.VISIBLE);
        this.child.current.pause();

        if (state !== PLAYER_STATE.SEEKING) {
            this.props.updatePlayerState(PLAYER_STATE.SEEKING);
        }

        this.props.updateSeekingSpeed(speed);

        console.log('[Player] seek speed: ' + speed);
        console.log('[Player] seek direction: ' + Object.keys(SEEKING_DIRECTION).find(key => SEEKING_DIRECTION[key] === direction));

        if (this._seekingInterval) {
            clearInterval(this._seekingInterval);
            this._seekingInterval = null;
        }

        if (direction === SEEKING_DIRECTION.REWIND) {
            console.log('[Player] rewind');

            this._seekingInterval = setInterval(() => {
                this._curTime -= 1000;

                if (this._curTime < 0) {
                    this._curTime = 0;
                    this.stopSeeking();
                }

                this.updateProgress();

                console.log('[Player] seek current time: ' + this._curTime);
            }, interval);
        }

        if (direction === SEEKING_DIRECTION.FASTFORWARD) {
            console.log('[Player] fastForward');

            this._seekingInterval = setInterval(() => {
                this._curTime += 1000;

                if (this._curTime > this.child.current.getDuration()) {
                    this.stopSeeking();
                }

                this.updateProgress();

                console.log('[Player] seek current time: ' + this._curTime);
            }, interval);
        }

    }

    stopSeeking = () => {
        console.log('[Player] stopSeeking');

        this.showControls();
        clearInterval(this._seekingInterval);
        this._seekingInterval = null;

        this.child.current.seek(this._curTime);
        this.startProgressInterval();
        this.props.updatePlayerState(PLAYER_STATE.PLAYING);
        this.props.updateSeekingSpeed(0);
    }

    stop() {
        console.log('[Player] stop');

        this.stopProgressInterval();
        this.clearControlsTimeout();
        this.child.current.stop();
        this.props.updatePlayerState(PLAYER_STATE.STOPPED);
        this.showPlay();
        this._curTime = 0;
    }

    exit() {
        console.log('[Player] exit');
        this.child.current.exit();
    }

    render() {
        return (
            <div>
                {this.renderPlayer()}

                <Controls
                    onPlay={() => this.play()}
                    onPause={() => this.pause()}
                    onFastForward={() => this.seek(SEEKING_DIRECTION.FASTFORWARD)}
                    onRewind={() => this.seek(SEEKING_DIRECTION.REWIND)}
                    onStopSeeking={() => this.stopSeeking()}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        player: state.player
    }
};

export default connect(mapStateToProps, {
    updatePlayerState,
    updateSeekingSpeed,
    updateControlsState,
    updateProgress,
    updateDuration
})(Player);
