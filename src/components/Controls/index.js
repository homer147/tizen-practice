import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faForward,
    faBackward,
    faPause,
    faPlay
} from '@fortawesome/free-solid-svg-icons';

import './styles.scss';
import config from '../../configs/appConfig';
import Progressbar from './Progressbar';

const PLAYER_STATE = config.playerState;
const CONTROLS_STATE = config.controlsState;

class Controls extends Component {
    componentDidMount() {
        ReactDOM.findDOMNode(this.refs.playPause).focus();
    }

    handleClickPlayPause = () => {
        const { player } = this.props;

        switch(player.state) {
            case PLAYER_STATE.PLAYING:
                this.props.onPause();
                break;

            case PLAYER_STATE.PAUSED:
            case PLAYER_STATE.STOPPED:
                this.props.onPlay();
                break;

            case PLAYER_STATE.SEEKING:
                this.props.onStopSeeking();
                break;
        }
    }

    handleFastForward = () => {
        this.props.onFastForward();
    }

    handleRewind = () => {
        this.props.onRewind();
    }

    render() {
        const { player } = this.props;
        const controlsClassName = `controls-container ${player.controlsState ? '' : 'hide'}`;
        const elementClassName = player.controlsState === CONTROLS_STATE.STOPPED ? 'hide' : '';
        let playPause;

        if (player.state === PLAYER_STATE.PLAYING) {
            playPause = <FontAwesomeIcon icon={faPause} />
        } else {
            playPause = <FontAwesomeIcon icon={faPlay} />
        }

        return (
            <div className={controlsClassName}>
                <div className={elementClassName}>
                    <Progressbar />
                </div>
                <div className="controls">
                    <div className={`rewind ${elementClassName}`} tabIndex="1" onClick={this.handleRewind}>
                        <FontAwesomeIcon icon={faBackward} />
                    </div>
                    <div className="play-pause" ref="playPause" tabIndex="2" onClick={this.handleClickPlayPause}>
                        {playPause}
                    </div>
                    <div className={`fast-forward ${elementClassName}`} tabIndex="3" onClick={this.handleFastForward}>
                        <FontAwesomeIcon icon={faForward} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        player: state.player
    }
};

export default connect(mapStateToProps)(Controls);
