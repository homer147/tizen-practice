import React, { Component } from 'react';
import { connect } from 'react-redux';

class ProgressBar extends Component {
    formatMsToTime(ms) {
        let secs = Math.floor((ms / 1000) % 60);
        let mins = Math.floor((ms / (1000 * 60)) % 60);
        let hrs = Math.floor((ms / (1000 * 60 * 60)) % 24);

        hrs = hrs < 10 ? '0' + hrs : hrs;
        mins = (mins < 10) ? '0' + mins : mins;
        secs = (secs < 10) ? '0' + secs : secs;

        return hrs + ":" + mins + ":" + secs;
    }

    formatProgressToPercentage() {
        const { progress, duration } = this.props;

        if (!duration) {
            return 0;
        }

        return (progress / duration * 100).toFixed(2)
    }

    render() {
        return (
            <div>
                <div className="duration">{`${this.formatMsToTime(this.props.progress)} / ${this.formatMsToTime(this.props.duration)}`}</div>
                <div className="progressbar-container">
                    <div className="progressbar" style={{ width: `${this.formatProgressToPercentage()}%` }}></div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        progress: state.progressbar.progress,
        duration: state.progressbar.duration
    }
}

export default connect(mapStateToProps)(ProgressBar);
