import React, { Component } from 'react';

const REQUIRED_KEYS = [
    "MediaPlayPause"
];

const MAP_KEYS = {
    "playPause": 10252,
    "back": 10009,
    "arrowLeft": 37,
    "arrowUp": 38,
    "arrowRight": 39,
    "arrowDown": 40,
    "enter": 13
};

class HTML5Player extends Component {
    constructor(props) {
        super(props);

        this._player = null;
    }

    componentDidMount() {
        this.registerKeys();
        this.load();

        this.supportsDRM();
    }

    supportsDRM() {
        const config = [{
            "initDataTypes": ["cenc"],
            "audioCapabilities": [{
                "contentType": "audio/mp4;codecs=\"mp4a.40.2\""
            }],
            "videoCapabilities": [{
                "contentType": "video/mp4;codecs=\"avc1.42E01E\""
            }]
        }];

        try {
            navigator.requestMediaKeySystemAccess("com.widevine.alpha", config).
                then(function(mediaKeySystemAccess) {
                    console.log('widevine support ok');
                }).catch(function(e) {
                    console.log('no widevine support');
                    console.log(e);
                });
        } catch (e) {
            console.log('no widevine support');
            console.log(e);
        }
        try {
        navigator.requestMediaKeySystemAccess("com.microsoft.playready", config).
            then(function(mediaKeySystemAccess) {
                console.log('playready support ok');
            }).catch(function(e) {
                console.log('no playready support');
                console.log(e);
            });
        } catch (e) {
            console.log('no playready support');
            console.log(e);
        }
    }

    registerKeys() {
        REQUIRED_KEYS.forEach(key => {
            try {
                console.log('registering key: ' + key);
                tizen.tvinputdevice.registerKey(key);
            } catch (e) {
                console.log(e.message);
            }
        });
    }

    load() {
        console.log('Device Player load');

        this._player.load();

        this._player.addEventListener('loadeddata', () => {
            console.log("Movie loaded.");
        });
        this._player.addEventListener('loadedmetadata', () => {
            console.log("Meta data loaded.");
        });
        this._player.addEventListener('timeupdate', () => {
            //console.log("Current time: " + this._player.currentTime);
            //progress.updateProgress(this._player.currentTime, this._player.duration);
        });
        this._player.addEventListener('play', () => {
            console.log("Playback started.");
        });
        this._player.addEventListener('pause', () => {
            console.log("Playback paused.");
        });
        this._player.addEventListener('ended', () => {
            console.log("Playback finished.");
        });
    }

    getMapKeys() {
        return MAP_KEYS;
    }

    getCurrentTime() {
        return this._player.currentTime * 1000;
    }

    getDuration() {
        return this._player.duration * 1000;
    }

    play() {
        this._player.play();
    }

    pause() {
        this._player.pause();
    }

    stop() {
        this._player.pause();
        this._player.currentTime = 0;
    }

    seek(time) {
        this._player.currentTime = time / 1000;
        this._player.play();
    }

    exit() {
        tizen.application.getCurrentApplication().hide();
    }

    render() {
        if (!this.props.video) {
            return 'no url provided';
        }

        return (
            <div className="player">
                <video style={{ width: '100%', display: 'block' }} ref={el => this._player = el}>
                    <source src={this.props.video.url} type={this.props.video.type} />
                </video>
            </div>
        );
    }
}

export default HTML5Player;
