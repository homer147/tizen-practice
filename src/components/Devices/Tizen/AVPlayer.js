import React, { Component } from 'react';

const REQUIRED_KEYS = [
    "MediaPlayPause"
];

const MAP_KEYS = {
    "playPause": 10252,
    "back": 10009,
    "arrowLeft": 37,
    "arrowUp": 38,
    "arrowRight": 39,
    "arrowDown": 40,
    "enter": 13
};

class AVPlayer extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.registerKeys();
        this.load(this.props.video);
    }

    registerKeys() {
        REQUIRED_KEYS.forEach(key => {
            try {
                console.log('registering key: ' + key);
                tizen.tvinputdevice.registerKey(key);
            } catch (e) {
                console.log(e.message);
            }
        });
    }

    setPlayready(video) {
        const params = {
            DeleteLicenseAfterUse: true
        };

        if (video.LicenseServer) {
            params.LicenseServer = video.LicenseServer;
        }

        console.log('setDrm Playready param: ' + JSON.stringify(params));

        try {
            webapis.avplay.setDrm("PLAYREADY", "SetProperties", JSON.stringify(params));
        } catch (e) {
            console.log(e.name + ' ' + e.message);
            console.log(e);
        }
    }

    load(video) {
        console.log('Device Player load url: ' + video.url);

        const { open, setDisplayRect, setListener } = webapis.avplay;
        const listener = {
            onbufferingstart: function () {
                console.log("Buffering start.");
            },
            onbufferingprogress: function (percent) {
                console.log("Buffering progress data : " + percent);
            },
            onbufferingcomplete: function () {
                console.log("Buffering complete.");
            },
            oncurrentplaytime: function (currentTime) {
                //console.log("Current playtime: " + currentTime);
            },
            onevent: function (eventType, eventData) {
                console.log("event type: " + eventType + ", data: " + eventData);
            },
            ondrmevent: function (drmEvent, drmData) {
                console.log("DRM callback: " + drmEvent + ", data: " + drmData);
            },
            onstreamcompleted: function () {
                console.log("Stream Completed");
                this.stop();
            }.bind(this),
            onerror: function (eventType) {
                console.log("event type error : " + eventType);
            }
        };

        if (!video.url) {
            return;
        }

        try {
            open(video.url);
            setDisplayRect(0, 0, 1920, 1080);
            setListener(listener);
        } catch (e) {
            console.log('cannot play: ' + e.message);
        }

        if (video.drm) {
            this.setPlayready(video);
        }
    }

    getState() {
        return webapis.avplay.getState();
    }

    getMapKeys() {
        return MAP_KEYS;
    }

    getCurrentTime() {
        return webapis.avplay.getCurrentTime();
    }

    getDuration() {
        return webapis.avplay.getDuration();
    }

    play() {
        const { getState, prepare, play } = webapis.avplay;
        const state = getState();

        if (state === 'IDLE') {
            prepare();
            play();
        } else if (state === 'READY' || state === 'PLAYING' || state === 'PAUSED') {
            play();
        }
    }

    pause() {
        const { getState, pause } = webapis.avplay;
        const state = getState();

        if (state === 'PLAYING' || state === 'PAUSED') {
            pause();
        }
    }

    seek(time) {
        const { getState, seekTo } = webapis.avplay;
        const state = getState();

        if (state === 'IDLE' || state === 'READY' || state === 'PLAYING' || state === 'PAUSED') {
            seekTo(time, () => {
                this.play();
            }, (e) => {
                console.log('Error while seeking: ' + e.message);
            });
        }
    }

    stop() {
        const { getState, stop } = webapis.avplay;
        const state = getState();

        if (state === 'NONE' || state === 'IDLE' || state === 'READY' || state === 'PLAYING' || state === 'PAUSED') {
            stop();
        }
    }

    exit() {
        tizen.application.getCurrentApplication().hide();
    }

    render() {
        return (
            <div className="player">
                <object tabIndex="-1" className="av-player" type="application/avplayer" style={{ position: 'absolute', width: '100%', height: '100%' }}></object>
            </div>
        );
    }
}

export default AVPlayer;
