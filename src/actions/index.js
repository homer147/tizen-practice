import {
    CHANGE_PLAYER_STATE,
    UPDATE_SEEKING_SPEED,
    CHANGE_CONTROLS_STATE,
    UPDATE_PROGRESS,
    UPDATE_DURATION
} from './types';

export const updatePlayerState = newPlayerstate => {
    return {
        type: CHANGE_PLAYER_STATE,
        payload: newPlayerstate
    };
}

export const updateSeekingSpeed = newSeekingSpeed => {
    return {
        type: UPDATE_SEEKING_SPEED,
        payload: newSeekingSpeed
    };
}

export const updateControlsState = newControlsState => {
    return {
        type: CHANGE_CONTROLS_STATE,
        payload: newControlsState
    };
}

export const updateProgress = newProgress => {
    return {
        type: UPDATE_PROGRESS,
        payload: newProgress
    };
}

export const updateDuration = duration => {
    return {
        type: UPDATE_DURATION,
        payload: duration
    };
}
