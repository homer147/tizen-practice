const config = {
    "mediaType": "mp4",
    "playerType": "HTML5Player",
    "playerState": {
        "STOPPED": 0,
        "PLAYING": 1,
        "PAUSED": 2,
        "SEEKING": 3
    },
    "seekingSpeed": [0, 4, 8, 16, 32],
    "controlsState": {
        "INVISIBLE": 0,
        "VISIBLE": 1,
        "STOPPED": 2
    },
    "seekingDirection": {
        "REWIND": 0,
        "FASTFORWARD": 1
    },
    video: {
        "hls": {
            "url": "https://kayoeventshlsts.akamaized.net/hls/live/2001961/591641NBALosAngelesLakersvDenverNuggets/master.m3u8?TestExec=true&hdnea=exp=1573343759~acl=/*~hmac=8956b1d8f328c1f50bb444f402e54ae736898400571f6f62b43e44b0cdf7a7f0",
            "LicenseServer": null,
            "type": "application/x-mpegURL",
            "drm": false
        },
        "mp4": {
            "url": "http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4",
            "LicenseServer": null,
            "type": "video/mp4",
            "drm": false
        },
        "dash": {
            "url": "https://amssamples.streaming.mediaservices.windows.net/622b189f-ec39-43f2-93a2-201ac4e31ce1/BigBuckBunny.ism/manifest(format=mpd-time-csf)",
            "LicenseServer": "https://amssamples.keydelivery.mediaservices.windows.net/PlayReady/",
            "type": "application/dash+xml",
            "drm": true
        }
    }
};

export default config;
