import {
    UPDATE_PROGRESS,
    UPDATE_DURATION
} from '../actions/types';

const INITIAL_STATE = {
    progress: 0,
    duration: 0
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case UPDATE_PROGRESS:
            return { ...state, progress: action.payload };

        case UPDATE_DURATION:
            return { ...state, duration: action.payload };

        default:
            return state;
    }
}
