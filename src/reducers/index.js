import { combineReducers } from 'redux';

import playerReducer from './playerReducer';
import progressbarReducer from './progressbarReducer';

export default combineReducers({
    player: playerReducer,
    progressbar: progressbarReducer
});
