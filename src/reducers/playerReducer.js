import {
    CHANGE_PLAYER_STATE,
    UPDATE_SEEKING_SPEED,
    CHANGE_CONTROLS_STATE,
    UPDATE_DURATION
} from '../actions/types';

const INITIAL_STATE = {
    state: 0,
    seekingSpeed: 0,
    controlsState: 0
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case CHANGE_PLAYER_STATE:
            return { ...state, state: action.payload };

        case UPDATE_SEEKING_SPEED:
            return { ...state, seekingSpeed: action.payload };

        case CHANGE_CONTROLS_STATE:
            return { ...state, controlsState: action.payload };

        default:
            return state;
    }
}
